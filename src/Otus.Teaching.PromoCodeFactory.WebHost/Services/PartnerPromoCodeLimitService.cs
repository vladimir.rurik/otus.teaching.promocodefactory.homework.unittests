﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    // Сервис для работы с лимитами промокодов партнеров
    public class PartnerPromoCodeLimitService : IPartnerPromoCodeLimitService
    {
        private readonly IRepository<Partner> _partnersRepository;

        public PartnerPromoCodeLimitService(IRepository<Partner> partnersRepository)
        {
            _partnersRepository = partnersRepository;
        }

        public async Task<(Guid partnerId, Guid newLimitId)> SetLimitAsync(Guid partnerId, int limit, DateTime endDate)
        {
            var partner = await _partnersRepository.GetByIdAsync(partnerId);

            if (partner == null)
                throw new NotFoundException("The specified partner could not be found.");

            if (!partner.IsActive)
                throw new BusinessException("This partner is currently inactive.");

            if (limit <= 0)
                throw new BusinessException("The limit must be greater than zero.");

            var activeLimit = partner.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue);

            if (activeLimit != null && DateTime.Now < endDate)
            {
                //Если партнеру выставляется лимит, то мы 
                //должны обнулить количество промокодов, которые партнер выдал, если лимит закончился, 
                //то количество не обнуляется
                partner.NumberIssuedPromoCodes = 0;

                //При установке лимита нужно отключить предыдущий лимит
                activeLimit.CancelDate = DateTime.Now;
            }

            var newLimit = new PartnerPromoCodeLimit()
            {
                Limit = limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = DateTime.Now,
                EndDate = endDate
            };

            partner.PartnerLimits.Add(newLimit);

            await _partnersRepository.UpdateAsync(partner);

            return (partner.Id, newLimit.Id);
        }
    }

}
