﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public interface IPartnerPromoCodeLimitService
    {
        Task<(Guid partnerId, Guid newLimitId)> SetLimitAsync(Guid partnerId, int limit, DateTime endDate);
    }
}