﻿using System;
using Xunit;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Services
{
    public class PartnerPromoCodeLimitServiceTests
    {
        private readonly Mock<IRepository<Partner>> _mockRepository;
        private readonly PartnerPromoCodeLimitService _service;

        public PartnerPromoCodeLimitServiceTests()
        {
            _mockRepository = new Mock<IRepository<Partner>>();
            _service = new PartnerPromoCodeLimitService(_mockRepository.Object);
        }

        [Fact]
        public async Task SetLimitAsync_WhenPartnerDoesNotExist_ShouldThrowNotFoundException()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            _mockRepository.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync((Partner)null);

            // Act & Assert
            await Assert.ThrowsAsync<NotFoundException>(() => _service.SetLimitAsync(partnerId, 10, DateTime.Now.AddDays(1)));
        }

        [Fact]
        public async Task SetLimitAsync_WhenPartnerIsNotActive_ShouldThrowBusinessException()
        {
            // Arrange
            var partner = new Partner { Id = Guid.NewGuid(), IsActive = false };
            _mockRepository.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act & Assert
            await Assert.ThrowsAsync<BusinessException>(() => _service.SetLimitAsync(partner.Id, 10, DateTime.Now.AddDays(1)));
        }

        [Fact]
        public async Task SetLimitAsync_WhenLimitIsLessThanOrEqualToZero_ShouldThrowBusinessException()
        {
            // Arrange
            var partner = new Partner { Id = Guid.NewGuid(), IsActive = true };
            _mockRepository.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act & Assert
            await Assert.ThrowsAsync<BusinessException>(() => _service.SetLimitAsync(partner.Id, 0, DateTime.Now.AddDays(1)));
        }

        [Fact]
        public async Task SetLimitAsync_WhenValid_ShouldAddNewLimit()
        {
            // Arrange
            var partner = new Partner
            {
                Id = Guid.NewGuid(),
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };
            var limit = 10;
            var endDate = DateTime.Now.AddDays(1);

            _mockRepository.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);
            _mockRepository.Setup(repo => repo.UpdateAsync(It.IsAny<Partner>()))
                .Returns(Task.CompletedTask)
                .Callback<Partner>(p => partner = p); // Simulate updating the partner object

            // Act
            await _service.SetLimitAsync(partner.Id, limit, endDate);

            // Assert
            Assert.Contains(partner.PartnerLimits, l => l.Limit == limit && l.EndDate == endDate);
            _mockRepository.Verify(repo => repo.UpdateAsync(It.IsAny<Partner>()), Times.Once);
        }
    }
}