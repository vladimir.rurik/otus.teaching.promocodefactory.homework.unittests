﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Dsl;
using FluentAssertions;
using FluentAssertions.Common;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Namotion.Reflection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Builder;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners {
    public class SetPartnerPromoCodeLimitAsyncTests {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly SetPartnerPromoCodeLimitRequest _partnersPromoCodeLimitRequest;
        private readonly Guid _partnerId;
        private readonly PartnerPromoCodeLimitService _service;
        private readonly Mock<IPartnerPromoCodeLimitService> _mockService;

        public SetPartnerPromoCodeLimitAsyncTests() {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
            _partnersPromoCodeLimitRequest = fixture.Build<SetPartnerPromoCodeLimitRequest>().Create();
            _partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            _service = new PartnerPromoCodeLimitService(_partnersRepositoryMock.Object);
            _mockService = new Mock<IPartnerPromoCodeLimitService>();
        }

        public Partner CreateBasePartner() {
            var partner = new Partner() {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Ńóļåščćšóųźč",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>() {
                    new PartnerPromoCodeLimit() {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2023, 12, 9),
                        EndDate = new DateTime(2024, 1, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        public PartnerPromoCodeLimit CreatePartnerPromoCodeLimit(DateTime date) {
            return new PartnerPromoCodeLimit() {
                Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                CreateDate = new DateTime(2023, 12, 9),
                EndDate = date,
                Limit = 100
            };
        }


        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            _mockService.Setup(service => service.SetLimitAsync(_partnerId, It.IsAny<int>(), It.IsAny<DateTime>()))
                .ThrowsAsync(new NotFoundException("The specified partner could not be found."));

            var partnersController = new PartnersController(_partnersRepositoryMock.Object, _mockService.Object);

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, new SetPartnerPromoCodeLimitRequest());

            // Assert
            result.Should().BeAssignableTo<NotFoundObjectResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            _mockService.Setup(service => service.SetLimitAsync(_partnerId, It.IsAny<int>(), It.IsAny<DateTime>()))
                .ThrowsAsync(new BusinessException("This partner is currently inactive."));

            var partnersController = new PartnersController(_partnersRepositoryMock.Object, _mockService.Object);

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, _partnersPromoCodeLimitRequest);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Theory]
        [InlineData(1)]
        [InlineData(-1)]
        public async void
            SetPartnerPromoCodeLimitAsync_RequestNotNull_NumberIssuedPromoCodes_is_0_And_RequestDateBeforeNow_NumberIssuedPromoCodes_TheSame(
                int months
            ) {
            // Arrange
            var partner = CreateBasePartner();
            int expectedNum = 10;
            partner.NumberIssuedPromoCodes = expectedNum;
            var requestEndDate = DateTime.Now.AddMonths(months);

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partnerId))
                .ReturnsAsync(partner);

            // Act
            if (months > 0)
            {
                await _service.SetLimitAsync(_partnerId, 5, requestEndDate); // Set limit to 5 for the purpose of the test
            }

            // Assert
            if (months > 0)
            {
                partner.NumberIssuedPromoCodes.Should().Be(0);
            }
            else
            {
                partner.NumberIssuedPromoCodes.Should().Be(expectedNum);
            }
        }

        [Fact]
        public async void
            SetPartnerPromoCodeLimitAsync_SetNewLimit_ReturnsBadRequest_ActiveLimitCancelDate_DateTimeNow() {
            // Arrange
            var partner = CreateBasePartner();
            var date = DateTime.Now.AddMonths(1);
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>() { CreatePartnerPromoCodeLimit(date) };
            var expectedDate = DateTime.Now.AddMonths(1).Month;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partnerId))
                .ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(_partnerId, _partnersPromoCodeLimitRequest);

            // Assert
            partner.PartnerLimits.ElementAt(0).EndDate.Month.Should().Be(expectedDate);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_SetNewLimit_LowerThanZero_ShouldThrowBusinessException()
        {
            // Arrange
            var partner = CreateBasePartner();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(_partnerId))
                .ReturnsAsync(partner);

            // Act & Assert
            var exception = await Assert.ThrowsAsync<BusinessException>(() =>
                _service.SetLimitAsync(_partnerId, -1, DateTime.Now.AddDays(1)));

            exception.Message.Should().Be("The limit must be greater than zero.");
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_ValidSave_SuccessUpdate()
        {
            // Arrange
            var partner = PartnersBuilder.CreateBasePartner(); // Assuming this creates a partner with a valid ID
            var partnerId = partner.Id;

            _partnersRepositoryMock
                .Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            await _service.SetLimitAsync(partnerId, _partnersPromoCodeLimitRequest.Limit, _partnersPromoCodeLimitRequest.EndDate);

            // Assert
            _partnersRepositoryMock.Verify(repo => repo.UpdateAsync(partner), Times.Once);
        }
    }
}